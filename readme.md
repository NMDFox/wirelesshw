Chip HW 28-PIN QFN

Bootloader command - p-load -w HEXFILE

= Helpful Links =

== Hardware Details == 

Pololu P-Star User's Guide
https://www.pololu.com/docs/0J62

== Circuits ==

Stepping down from 5V to 3.3
https://hackaday.com/2016/12/05/taking-it-to-another-level-making-3-3v-and-5v-logic-communicate-with-level-shifters/#:~:text=The%20simplest%20possible%20step%2Ddown,from%20an%20applied%205V%20input.

Resistor colour band calculator
https://www.digikey.com/en/resources/conversion-calculators/conversion-calculator-resistor-color-code-4-band

NRF24L01 Wireless Module details
https://lastminuteengineers.com/nrf24l01-arduino-wireless-communication/

