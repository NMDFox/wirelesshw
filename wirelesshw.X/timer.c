/*******************************************************************************
* @file timer.c
* @author Niall Mullins
* @date 26/10/20
* @brief This file holds to configuration required to generate a 1ms tick
*
*******************************************************************************/
#include "hardware.h"

void Timer1_delay()
{
    /* Enable 16-bit TMR1 register, No pre-scale, internal clock,timer OFF */
    T1CON=0x02;

    TMR1=0xf846;		/* Load count for generating delay of 1ms */
    T1CONbits.TMR1ON=1;  	/* Turn ON Timer1 */

    while(PIR1bits.TMR1IF==0); 	/* Wait for Timer1 overflow interrupt flag */

    TMR1ON=0;  			/* Turn OFF timer */
    TMR1IF=0;  			/* Make Timer1 overflow flag to '0' */
}