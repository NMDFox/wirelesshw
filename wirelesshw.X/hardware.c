/*******************************************************************************
* @file hardware.c
* @author Niall Mullins
* @date 25/09/20
* @brief This is the main C file used to hold any configuration routines that 
* may be required to configure the microcontroller 
* 
*******************************************************************************/

#include "hardware.h"

/*******************************************************************************
* @brief A basic function to move the pin initialisation out of the main
* function. This function has no parameters and returns nothing
*
*******************************************************************************/
void HardwareInitialisation(void)
{
    // Configure hardware pins
    /* =====================
    LATA0 - Unused
    LATA1 - Unused
    LATA2 - Unused
    LATA3 - Unused
    LATA4 - Unused
    LATA5 - Unused
    LATA6 - Unused
    LATA7 - Unused
    ======================== */
    LATA = 0x00;
    TRISA = 0x00;
    
    /* =====================
    LATB0 - Unused
    LATB1 - Unused
    LATB2 - Unused
    LATB3 - Unused
    LATB4 - Unused
    LATB5 - Unused
    LATB6 - Yellow LED
    LATB7 - Green LED
    ======================== */
    LATB = 0xC1;
    TRISB = 0x00;
    
    /* =====================
    LATC0 - Unused
    LATC1 - Unused
    LATC2 - Unused
    LATC3 - Unused
    LATC4 - Unused
    LATC5 - Unused
    LATC6 - Red LED
    LATC7 - Unused
    ======================== */    
    LATC = 0x00;
    TRISC = 0x00;        

    // Clock initialisation
    OSCCON=0x72;  		/* Configure oscillator frequency to 8MHz */
    TRISB=0;  			/* Set as output port */
    Pulse=0xff;  		/* Send high on PortB */
}
