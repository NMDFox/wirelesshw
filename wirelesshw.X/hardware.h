/*******************************************************************************
* @file hardware.h
* @author Niall Mullins
* @date 25/09/20
* 
*******************************************************************************/
#ifndef HARDWARE_H
#define	HARDWARE_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include "timer.h"

#define Pulse LATB  		/* Define Pulse as LATB to output on PORTB */

void HardwareInitialisation(void);

#ifdef	__cplusplus
}
#endif

#endif	/* HARDWARE_H */

