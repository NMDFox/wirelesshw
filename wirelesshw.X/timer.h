/*
 * File:   timer.h
 * Author: CINOS
 *
 * Created on October 26, 2020, 2:38 PM
 */

#ifndef TIMER_H
#define	TIMER_H

#ifdef	__cplusplus
extern "C" {
#endif


void Timer1_delay();

#ifdef	__cplusplus
}
#endif

#endif	/* TIMER_H */

